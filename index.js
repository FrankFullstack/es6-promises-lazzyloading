import data from './sample_data/data.json';
import { PromisesDataQuery } from './PromiseDBQuery'

//Create an instance of the class to query POSTs
const query = new PromisesDataQuery(data, 'post');

//Getting data
query.getAllData()
    .then((all) => {
        console.log('All data:', all);
    });

//Getting data by Id
query.getDataById(3)
    .then((item) => {
        return query.getWithRelation(item, 'author');
    }).then((finallData) => {
        console.log('Filtered and related data:', finallData);
    });