# ES6-Promises-LazzyLoading

Lazzy Loading and some relational capabilities in NoSQL using Promises based on the WesBos ES6.io series https://es6.io. A ES6 series that I totally recommend and authored by [@wesbos](https://twitter.com/wesbos)

## Sample data:

You may find some sample JSON formatted data in the `data.json` file located in the `sample_data` folder.

```javascript
{
    "data": {
        "post": [
            { "id": 1, "title": "This is my first post", "author": 1},
            { "id": 2, "title": "This is my second post", "author": 1},
            { "id": 3, "title": "This is my third post", "author": 2},
            { "id": 4, "title": "This is my fourth post", "author": 2},
            { "id": 5, "title": "This is my fifth post", "author": 3}
        ],
        "author": [
            { "id": 1, "name": "Author1", "twitter": "@author1"},
            { "id": 2, "name": "Author2", "twitter": "@author2"},
            { "id": 3, "name": "Author3", "twitter": "@author3"}
        ]
    }
}
```

## Execution sample code:

Firstly you need to execute the following command:

```
$ npm install
```

In order to install all the project dependencies.

After this first step, you may run the `npm start` script command in your command line interface and modify as you need the data source `data.json` and the sample query in the `index.js` file.

This script execute the sample code:

```javascript
import data from './sample_data/data.json';
import { PromisesDataQuery } from './PromiseDBQuery'

//Create an instance of the class to query POSTs
const query = new PromisesDataQuery(data, 'post');

//Getting data
query.getAllData()
    .then((all) => {
        console.log('All data:', all);
    });

//Getting data by Id
query.getDataById(3)
    .then((item) => {
        return query.getWithRelation(item, 'author');
    }).then((finallData) => {
        console.log('Filtered and related data:', finallData);
    });
```

## PromiseDBQuery class:

This file contains the main ES6 Promise based class to solve some Database or JSON Object queries.

```javascript
/*
Promises based Database Queries
*/
export class PromisesDataQuery {
    constructor(source, filter) {
        this.dataSource = source.data;
        this.primaryFilter = filter;
    }

    getAllData(filter) {
        
        const items = this.dataSource[this.primaryFilter];

        return new Promise((resolve, reject) => {
            items ? resolve(items) : reject(Error(`An error occurred retrieving the data from the source`));
        });
    }

    getDataById(id) {
        const items = this.dataSource[this.primaryFilter];

        return new Promise((resolve, reject) => {
            const item = items.find((item) => item.id === id);
            item ? resolve(item) : reject(Error(`The item was not found with the id: ${id}`));
        });
    }

    getWithRelation(item, relation) {
        return new Promise((resolve, reject) => {
            const relationDetails = this.dataSource[relation].find((value) => value.id === item[relation]);
            if (relationDetails) {
                item[relation] = relationDetails;
                resolve(item);
            } else {
                reject(Error(`Error getting the relationship`));
            }
        })
    }
}
```

## Notice: 
> This project is for educational purpose for the ES6 Features learning. Enjoy it!

## License:

MIT