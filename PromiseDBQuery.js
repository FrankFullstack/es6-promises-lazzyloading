
/*
Promises based Database Queries
*/
export class PromisesDataQuery {
    constructor(source, filter) {
        this.dataSource = source.data;
        this.primaryFilter = filter;
    }

    getAllData(filter) {
        
        const items = this.dataSource[this.primaryFilter];

        return new Promise((resolve, reject) => {
            items ? resolve(items) : reject(Error(`An error occurred retrieving the data from the source`));
        });
    }

    getDataById(id) {
        const items = this.dataSource[this.primaryFilter];

        return new Promise((resolve, reject) => {
            const item = items.find((item) => item.id === id);
            item ? resolve(item) : reject(Error(`The item was not found with the id: ${id}`));
        });
    }

    getWithRelation(item, relation) {
        return new Promise((resolve, reject) => {
            const relationDetails = this.dataSource[relation].find((value) => value.id === item[relation]);
            if (relationDetails) {
                item[relation] = relationDetails;
                resolve(item);
            } else {
                reject(Error(`Error getting the relationship`));
            }
        })
    }
}